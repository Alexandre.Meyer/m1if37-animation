# Code pour les TP d'animation de personnage en C++ (en M1)




## Les fichiers à recupérer

Le code se base sur gkit2light (une branche) et se récupère avec Git comme ceci:
```
    git clone -b master_CharAnim https://forge.univ-lyon1.fr/Alexandre.Meyer/gkit2light.git gkit2light-master_CharAnim
```
qui fait un clone du dépôt gkit2light, branche `master_CharAnim` et range le tout dans le répertoire `gkit2light-master_CharAnim`
ou alors 
```
    git clone https://forge.univ-lyon1.fr/Alexandre.Meyer/gkit2light.git gkit2light-master_CharAnim
    cd gkit2light-master_CharAnim
    git checkout master_CharAnim
    => le update avec un nom de branche permet de passer dans une autre branche
```


## Pour compiler sous Linux avec codeblocks

```
    cd gkit2light-master_CharAnim
    premake5 --file=master_CharAnim.lua codeblocks
```
puis ouvrir le projet  `master_CharAnim.cbp`


## Pour compiler sous Linux avec make
```
    cd gkit2light
    premake5 --file=master_CharAnim.lua gmake
    make -f master_CharAnim.make ou juste make
    bin/master_CharAnim
```


## Pour compiler sous Linux de chez vous (ubuntu)

* [voir le apt-get install ... ici](http://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/html/group__installation.html)
* `sudo apt install premake4 premake5`
* puis comme indiqué plus haut


## Pour compiler sous Windows chez vous avec VSCode ou CodeBlocks ou VisualStudio

* Installez Git
* Installez Codeblocks ou VSCode avec Mingw OU Visual Studio
* Explorateur de fichier, placez vous dans le répertoire de vos fichiers, bouton droit "git bash here" puis le clone indiqué plus haut ...
* [Téléchargez les librairies (SDL,Glew, etc.) pour Windows ici](https://perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/M1IMAGE/extern_mingw_visual.zip)
  * Copiez le répertoire 'extern' dans le répertoire principal de gkit2light
  * Vous devez aussi copier toutes les .dll du répertoire extern/mingw/lib OU extern/visual20??/lib dans gkit2light-master_CharAnim/bin (répertoire des exécutables), sinon vous aurez des erreurs à l’exécution des programmes
* Lancez le script `gkit2light-master_CharAnim/premake/premake-CharA.bat` qui va créer les projets makefile/codeblocks/visual` dans le répertoire gkitlight
* Ouvrez le projet `gkit2light-master_CharAnim/gkit2light.cbp` ou `gkit2light-master_CharAnim/build/gkit2light.sln`



## gKitLight la doc

[La documentation est ici.](http://jean-claude.iehl.perso.univ-lyon1.fr/Public/educ/M1IMAGE/index.html)


## Quelques explications sur le code

* 'h' pour l'aide
* 'a'=affiche/enlève les axes; 'g'=affiche/enlève la grille;
* souris bouton gauche= rotation du monde
* souris bouton droit et translation horizontale : pour zoomer
* **Votre code prendra place dans le module src/master_CharAnim/CharAnimViewer.h / .cpp**

* La class CharAnimViewer contient les fonctions init/draw qui s'occupent de l'affichage, elle dispose par héritage de fonction draw_sphere et draw_cylinder
* Pour ajouter des fichiers au projet, regarder le master_CharAnim.lua dans la racine
