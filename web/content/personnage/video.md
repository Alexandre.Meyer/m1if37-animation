
# CM Animation de personnages virtuels (Master 1ère année)

  * CM = Cours Magistral
  * Vous pouvez très facilement faire une pause au moment des
    exercices, revenir en arrière et/ou accélérer la vitesse de la vidéo
    depuis les options de YouTube (x 1.50 ou plus vite)


## CM1 : Animation basée squelette
Il y a 3 vidéos pour le CM1 :
     * L'introduction
     * Un exercice d'affichage de squelette animé très simple
     * Afficage récursif d'un squelette à partir de la structure de données, présentation du format BVH, prise en main du code du TP

#### CM1.a Introduction

<iframe width="560" height="315" src="//www.youtube.com/embed/TNSesEPrAn0 " frameborder="0" allowfullscreen></iframe>

#### CM1.b Exercice

<iframe width="560" height="315" src="//www.youtube.com/embed/1Js-9r28T-c" frameborder="0" allowfullscreen></iframe>

#### CM1.c Arbre/Squelette, Affichage récursif et BVH

<iframe width="560" height="315" src="//www.youtube.com/embed/1Y8VE9WrSfw" frameborder="0" allowfullscreen></iframe>


## CM2 : Edition d\'animations et Contrôle d\'un Personnage Virtuel

<iframe width="560" height="315" src="//www.youtube.com/embed/DfsH9bIIisA" frameborder="0" allowfullscreen></iframe>

## CM3 : Animation et Capture de Mouvements

<iframe width="560" height="315" src="//www.youtube.com/embed/7t98WLxikOo" frameborder="0" allowfullscreen></iframe>
