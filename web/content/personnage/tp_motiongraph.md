## TP 3e partie : graphe d'animation

 - [Motion Graph de l'article original](http://www.cs.wisc.edu/graphics/Gallery/kovar.vol/MoGraphs/);
 - Des BVH avec squelette compatible pour le graphe sont donné dans le git, répertoire Second_Life.\

Nous avons remarqué dans la partie 1 que la transition d'animation ne fonctionne bien que lorsque les deux poses du squelette sont assez
proches (il faut bien sûr également que les deux squelettes aient la même topologie). L'idée d'un graphe d'animation est de construire un
graphe où chaque noeud correspond à une pose d'une animation et où chaque arrête définit qu'une transition est possible entre les deux poses.



#### Comparaison de deux poses d'animation

Pour construire un graphe d'animation à partir d'une ou plusieurs animations, on doit être capable de comparer deux poses d'animation.
Une distance de 0 indique que les deux poses sont identiques. Une distance grande indique que les 2 poses sont très différentes.

A partir de la classe Skeleton, écrivez la fonction de calcul de distance entre deux poses de squelette. Cette fonction est déjà présente
dans la classe Skeleton plus haut mais en commentaire. Cette fonction calcule itérativement sur toutes les articulations la somme des distances euclidienne entre chaque articulation de deux squelettes aillant la même topologie mais dans des poses différentes.

```
    friend float Skeleton::Distance(const Skeleton& a, const Skeleton& b);
```

** Remarque ** : il est important de ne pas tenir compte de la translation et de la rotation de l'articulation racine. Une même pose a
deux endroits du monde doit donner une distance de 0. Dans un 1er temps, votre personnage aura son noeud root centré en (0,0,0), puis dans la dernière partie de cette question, vous traiterez le centre de gravité.



#### Construction du graphe

Ecrivez un module MotionGraph qui contiendra un ensemble de BVH et le graphe d'animation définissant des transitions dans cette ensemble d'animation.
     * Un noeud du graphe=(Identifiant d'une animation + un numéro de pose); 
     * un arc du graphe entre deux poses indique la transition possible entre ces deux poses. Deux poses sont compatibles à la transition quand la distance entre les deux squelettes sont inférieurs à un certain seuil fixé empiriquement.


Vous pouvez créer un module `CACore/CAMotionGraph.h/.cpp`

```
    class MotionGraph
    {
       ...
    protected:
       //! L'ensemble des BVH du graphe d'animation
       std::vector<BVH> m_BVH;

       //! Un noeud du graphe d'animation est repéré par un entier=un identifiant
       typedef int GrapheNodeID;

       //! Une animation BVH est repérée par un identifiant=un entier 
       typedef int BVH_ID;
       
       //! Un noeud du graphe contient l'identifiant de l'animation, le numéro 
       //! de la frame et les identifiants des noeuds successeurs 
       //! Remarque : du code plus "joli" aurait créer une classe CAGrapheNode
       struct GrapheNode
       {
         BVH_ID id_bvh;
         int frame;
         std::vector<GrapheNodeID> ids_next;     //! Liste des nœuds successeurs 
       };


       //! Tous les noeuds du graphe d'animation
       std::vector<GrapheNode> m_GrapheNode;

    };
```



#### Navigation dans le graphe

Une fois ce graphe construit, on peut définir différente manière de
naviguer dedans :
 - Un parcours aléatoire dans le graphe (juste pour vérifier que le graphe est ok);
 - L'utilisateur donne des directions au clavier => le parcours dans le graphe est conditionné par ces contraintes.



#### Gestion correcte du centre de gravité

Pour chaque arc du graphe, vous devez stocker la transformation (soit une matrice 4x4, soit un quaternion et une translation) du noeud root (souvent le centre de gravité) entre la pose i et la pose i+1. Cette transformation sera appliqué au noeud root de votre personnage quand il empruntera l'arc.

