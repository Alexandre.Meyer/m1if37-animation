# Master 1 Informatique - UE M1if37 Animation en synthèse d'image (3 ECTS)

## Contrôle de mouvement

Télécharger [les transparents du cours](../doc_controle/M1IF37_CM_CONTROLEUR.pdf)


Télécharger [l'énoncé du TP](../doc_controle/M1IF37_TP_CONTROLEUR.pdf)


Accéder [aux ressources pour le TP](../doc_controle/tp.zip)


Télécharger [le modèle pour le rapport](../doc_controle/M1IF37_TP_CONTROLEUR-modele_rapport.docx)		