# Master 1 Informatique - UE M1if37 Animation en synthèse d'image (3 ECTS)

Responsables de l'enseignement : [Alexandre Meyer](	
http://liris.cnrs.fr/alexandre.meyer), [Nicolas Pronost](	
http://liris.cnrs.fr/nicolas.pronost) et [Florence Zara](	
http://liris.cnrs.fr/florence.zara) - LIRIS, Université Lyon 1

Volume horaire : 10h30 CM, 19h30 TP


![im_all.png](images/im_all.png)

## Objectif de l'UE
<p style="text-align:justify;">M1if37 est une UE optionnelle de la 1ère année du Master d'Informatique de l'Université Lyon 1. Les cours ont lieu au semestre 2 (printemps). L'objectif de l'UE est de donner les bases de l'animation en synthèse d'images. Nous aborderons les deux grandes familles de méthodes. L'animation basée sur des données, par exemple pour l'animation d'humain virtuel (données issues de capture de mouvement). Et l'animation basée sur un modèle physique pour la simulation de phénomènes naturels comme le mouvement de textiles ou de fluide. L'UE laissera une grande part à l'application pratique avec la réalisation de TPs en C++/OpenGL proposant d'animer par exemple des humains virtuels, des vêtements, des cordes, une surface d'eau, etc.</p>

[Les slides de la présentation des options sont ici.](doc/M1if37_PresOption.pdf)


## Thématiques abordées

### Animation par modèles physiques (F. Zara) - 4h30 CM, 6h45 TP
  * Concepts physiques (forces, lois de Newton)
  * Méthodes d'intégration numérique
  * [La page web de cette partie](https://perso.liris.cnrs.fr/fzara/Web/M1Animation.html)


### Animation de personnage (A. Meyer) - 4h30 CM, 6h45 TP
  * Animation basée squelette
  * Graphe d'animation
  * Capture de mouvement
  * [La page web de cette partie](personnage)


### Contrôle de mouvement (Nicolas Pronost) - 1h30 CM, 6h TP
  * Mouvement d'objets rigides articulés
  * [La page web de cette partie](controle)


## Emploi du temps 2024-2025

![documents/M1if37_edt.png](doc/M1if37_edt.png)

  * Cours en salle TD005 Nautibus

  * TP en salles TP121, TP123 Nautibus

## Modalités de contrôle des connaissances (MCC)
   * **1 note de CCF** portant sur les 3 parties du cours
   * **3 notes de TP** : TP F. Zara, TP A. Meyer, TP N. Pronost (code + rapport + démo ou vidéo)

   * **Dates des évaluations** : 
     * Examen écrit : **mercredi 28 mai 2025, 8h à 9h30** en salle C008 Nautibus
     * Démo de TP : **mercredi 28 mai 2025, de 9h45 à 13h** en TP121, TP123 Nautibus
     * Date limite de rendu des 3 archives : **mercredi 28 mai 2025 13h**

  * **Modalité de rendu des TPs :** <p style="text-align:justify;">
     * Une archive sera à déposer sur TOMUSS (dans les 3 colonnes correspondantes). Cette archive contiendra le code du TP + un rapport.
     * Nous vous demandons également de mettre dans les 3 autres colonnes correspondantes de TOMUSS, l'URL pour accéder à une vidéo de votre projet, si vous ne faites pas de démo (car nous ne le compilerons pas forcément, donc il faut montrer tout votre travail).</p>

