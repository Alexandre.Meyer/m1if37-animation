# Site web et ressources de l'UE UE M1if37 Animation en synthèse d'image

[La page de l'UE est ici](http://alexandre.meyer.pages.univ-lyon1.fr/m1if37-animation/)




## Explication de la génération

Le site web est désormais fabriqué par ```Hugo``` (thème [congo](https://jpanther.github.io/congo/)). les sources se trouvent dans le répertoire ```web```.
Le site web est mis à jour par intégration continue (CI/CD) à chaque fois que vous faites un push (rien besoin d'autre, à part attendre quelques secondes). Le script d'intégration continue est ```.gitlab-ci.yml```.  Pour voir le résultat du script de génération, [allez ici](https://forge.univ-lyon1.fr/Alexandre.Meyer/m1if37-animation/-/jobs) ou depuis l'interface dans CI/Jobs.

Le fichier ```site/config.toml``` permet de configurer la génération du site. Mais normalement il n'y a pas besoin d'y toucher.
   * Les pages web sont générées à partir du répertoire ```web/content```. 
   * La page principale du site est ```web/content/_index.html```. Il faut bien laissé le ```_```, il indique qu'il y a des sous-répertoires 
   * ```web/content/controle``` pour la partie "Conrtôle d'animation". ```web/content/controle/index.md``` produit la page de cette partie.
   * ```web/content/personnage``` pour la partie "Animation de personnage virtuel". 
   * ```web/static``` : les fichiers autres (pdf, images, sujets, etc.) sont à ranger dedans. Par exemple, il y a 
      * ```web/static/images``` pour les images du site;
      * ```web/static/doc``` documents généraux de l'UE;
      * ```web/static/doc_controle``` documents de la partie contrôle (NP);
      * ```web/static/doc_charanim``` documents de la partie personnage (AM);


## Si Fork
Par défaut, gitlba ajoute de '-' un peu partout, vous pouvez changer l'adresse web dans Settings/General/Advanced/Change path.



## Tester le site en local
Pour tester vos mises à jour en local :
   * installer hugo : ```sudo apt install hugo```
   * dans le répertoire web, faire ```hugo serve```
   * dans votre navigateur, entrez l'url ```localhost:8000```

Vous pouvez aussi essayer de contruire le site en static en faisant juste ```hugo``` : le site sera constuit dans le répertoire public.


Toutes les infos sont détaillées ici !
Pour convertir du DOKUWIKI en Markdown, on peut utiliser pandoc??? (todo) ou certains web en ligne.
